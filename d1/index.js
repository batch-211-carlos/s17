console.log("Hello World");
//Functions 
	//Functions in Javascript are lines/blocks of code that tell our device/application to perform a certain task when called/invoked
	//Functions are mostly created to create a complicated tasks to run several lines of code in succession
	//They are also used to prevent repeating lines/blocks of codes that perform the same task/function

	//Function Declarations
		//(function statement) defines a function with the specified parameters

		/*
			Syntax:

			function functionName() {
				code block (statement)
			}
		*/

		//function keyword - used to define a Javascript function
		//functionName - the function name. Functions are named to be able to use a later in the code.
		//function block ({}) - the statement which comprise the body of the function. This is where the code is to be executed.
		//We can assign a variable to hold a function, but that will be explained later

		function printName() {
			console.log("My name is John.");
		};

		printName();

		//semicolons are used to seperate executable jaavascripts statements

		//Function Invocation
			//the code block and statements inside our function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called
			//it is common to use the term "call a function" instead of "invoke a function"

			printName();

			/*declaredFunction();*/ //result in an error //much like variables, we cannot invoke a function we have yet to define

		//Function Declations vs Expressions

			//Function Declarations
				//a function can be created through function declaration by using the function keyword and adding a function name
				//declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (call upon)

				declaredFunction(); //declared functions can be hoisted as long as the function has been defined.

				function declaredFunction() {
					console.log("Hello World from declaredFunction()");
				}

				//Note" Hoisting is JS's behavior for certain variables and functions to run or use them before their declaration

				declaredFunction();

			//Function Expression
				//a functioncan also be stored inside a variable which are "let", "const". This is called a function expression
				//a function expression is an anonymous function assigned to the variable function

				/*variableFunction();*/ //error - function expression, being stored in a let or const cannot be hoisted

				//Anonymous Function - function without a name

				let variableFunction = function(){
					console.log("Hello Again");
				};

				variableFunction();

				//we can also create a function expression of a named function
				//however, to invoke the function expression, we invoke it by its variable name, not by its function name
				//Function expressions are always invoked (called) using the variable name
				/*funcName();*/ //error

				let funcExpression = function functionName() {
					console.log("Hello from the other side");
				};

				funcExpression();

				//you can reassign declared function and function expression to new anonymous functions

				declaredFunction = function(){
					console.log("Updated declaredFunction");
				};

				declaredFunction();

				funcExpression = function(){
					console.log("updated funcExpression");
				};

				funcExpression();

				const constantFunc = function(){
					console.log("Initialized with Const");
				};

				constantFunc();

				//We cannot reassign a function expression initialized with const

				/*const constantFunc = function(){
					console.log("cannot be reassigned");
				};

				constantFunc();*/ //error


			//Function Scoping

			/*
			Scope is the accessibility (visibility) of variables within our program
			JS Variables has 3 types of scope:
			1. local/block scope
			2. global scope
			3. function scope
			*/

			/*let globalVar = "Mr. Worldwide";
			console.log(globalVar);*/ 
			//will work if it is invoked first

			{
				let localVar = "Armando Perez";
				console.log(localVar);
				/*console.log(globalVar);*/ //error 
				//we cannot invoke a globalVar inside a block if it is not invoked
			}

			let globalVar = "Mr. Worldwide";
			console.log(globalVar);
			//localVar being inside a block, cannot be accessed outside of its code block
			/*console.log(localVar);*/ //error

			//Function Scope

			/*
				JS has a function scope: Each function creates a new scope 
				Variables defined inside a function are not accessible (visible) from outside the function
				Variables declared with var, let , and const are quite similar when declared inside a function
			*/

			function showNames(){
				
				//Function scope anything that was inside the curly braces

				//function scope variables
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet = "Jane";

				console.log(functionVar);
				console.log(functionConst);
				console.log(functionLet);
			};

				showNames(); //functionName

				/*console.log(functionVar); //error 
				console.log(functionConst); //error 
				console.log(functionLet);*/ //error 

			/*
				the variables, functionVar, functionConst and functionLet are function scoped and cannot be accessed outside of the function they were declared
			*/


			//Nested Functions
				//You can create another function inside a function
				//This is called a nested function

				function myNewFunction (){
					let name = "Cee";

					function nestedFunction(){
						let nestedName = "Thor";
						console.log(name);
						console.log(nestedName);
					}
					/*console.log(nestedName);*/ //error
					//nestedName is not defined
					//nestedName variable, being declared in a nestedFunction cannot be accessed outside of the function it was declared in. because it is on the outside scope
					nestedFunction();
				}

				myNewFunction();

				/*nestedFunction();*/ //error
				/*
				since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in
				*/

				//Function and Global Scope Variables

				let globalName = "Nej";

				function myNewFunction2(){
					let nameInside = "Martin";
					//Variables declared globally (outside any function) have Global Scope
					//Global variables can be accessed from anywhere in a javascript program including from inside our function
					console.log(globalName);
				}

				myNewFunction2();


//Using Alert()

	//alert() allows us to show a small window at the top of our browser page to show information to our users
	//as opposed to a console.log() which will only show the message on the console
	//it allows us to show a short dialog or instruction to our user.
	//the page will wait until the user dismisses the dialog

	alert("Hello World"); //This will run immediately when the page loads

	//alert() syntax
	//alert ("<messageInString");
	//you can do it in numbers too

	//You can also use an alert to show a message to the user from a later function invocation

	function showSampleAlert(){
		alert("Hello, User!")
	};
	showSampleAlert();
/*	showSampleAlert();
	showSampleAlert();
	showSampleAlert();
	showSampleAlert();*/
	//Notes on the use of alert();
		//show only an alert() for short dialogs/messages to the user
		//do not overuse alert() because the program/js has to wait for it to be dismissed before continuing 

	//you will find that the page waits for the user to dismiss the dialog before proceeding
	//you can witness this by reloading while the console is open
	console.log("I will only log in the console when the alert is dismissed");

	//Using prompt()

		//prompt() allows us to show small window at the top of our browser to gather user input
		//it much like alert(), will have the page wait until the user completes or enters their input
		//the input from the prompt() will be returned as a String once the user dismisses the window

		let samplePrompt = prompt('Enter your Name.');
		console.log("Hello, " + samplePrompt);

		/*
		prompt() syntax:

		prompt("<dialogInString");

		*/

		let sampleNullPrompt = prompt("Don't enter anything");
		console.log(sampleNullPrompt);
		//prompt() returns an empty string when there is no input 
		//null if the user cancels prompt()

		//prompt() can be used for us to gather user input and be used in our code
		//However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused

		//prompt() used globally will be run immediately, so for better user experience, it is much better to use them accordingly or add them in a function

		function printWelcomeMessage(){
			let firstName = prompt("Enter your first name");
			let lastName = prompt("Enter your last name");
			console.log("hello, " + firstName + " " + lastName + "!");
			console.log("Welcome to my page!");
		};

		printWelcomeMessage();

		//Function Naming Conventions

		//function names should be definitive of the task it will perform

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		};

		getCourses();

		//Avoid generic Names to avoid confusion within your code

		function get (){
			let name = "Jamie";
			console.log(name);
		}

		get();

		//Avoid pointless and inappropriate function names

		function pikachu(){
			console.log(25%5);
		};

		pikachu();

		//Name your functions in small caps. Follow camelcase when naming variables and functions

		function displayCarInfo(){
			console.log("Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		};

		displayCarInfo();
