console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printWelcomeMessage(){
			let printfullName = prompt("Enter Full Name:");
			let printAge = prompt("What is your Age?");
			let printLocation = prompt("Where do you live?");
			alert("Thank you for your input!");
			console.log("Hi " + printfullName + "!");
			console.log("You are " + printAge + " years old.");
			console.log("You live in " + printLocation);
		};

		printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printFavoriteBands() {
		let showFavoriteRiverMaya = "1. Rivermaya"
		let showFavoriteParokyaNiEdgar = "2. Parokya Ni Edgar"
		let showFavoriteSouthBorder = "3. South Border"
		let showFavoriteEraserHeads = "4. Eraserheads"
		let showFavoriteSixPartInvention = "5. Six Part Invention"
			console.log(showFavoriteRiverMaya);
			console.log(showFavoriteParokyaNiEdgar);
			console.log(showFavoriteSouthBorder);
			console.log(showFavoriteEraserHeads);
			console.log(showFavoriteSixPartInvention);
			/*console.log("1. Rivermaya");
			console.log("2. Parokya Ni Edgar");
			console.log("3. South Border");
			console.log("4. Eraserheads");
			console.log("5. Six Part Invention");*/
		};

		printFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	/*function printMovieRatings() {
		let showLordOftheRings = "1. The Lord Of The Rings - The Tweo Towers"
		let showSilenceoftheLamb = "2. Silence of the Lamb"
		let showDearJohn = "3. Dear John"
		let showHarryPotter = "4. Harry Potter and the Half-Blood Prince"
		let showDayAfterTomorrow = "5. The Day After Tomorrow"
			console.log(showLordOftheRings);
			console.log("Rotten Tomatoes Rating: 95%");
			console.log(showSilenceoftheLamb);
			console.log("Rotten Tomatoes Rating: 95%");
			console.log(showDearJohn);
			console.log("Rotten Tomatoes Rating: 28%");
			console.log(showHarryPotter);
			console.log("Rotten Tomatoes Rating: 84%");
			console.log(showDayAfterTomorrow);
			console.log("Rotten Tomatoes Rating: 45%");
			
		};

		printMovieRatings();*/

		function printMovieRatings() {
			let showLordOftheRings = "1. The Lord Of The Rings - The Tweo Towers"
			let showSilenceoftheLamb = "2. Silence of the Lamb"
			let showDearJohn = "3. Dear John"
			let showHarryPotter = "4. Harry Potter and the Half-Blood Prince"
			let showDayAfterTomorrow = "5. The Day After Tomorrow"

					function showRatings(){
						let printAboveRatings = '95%';
						let printEqualRatings = '95%';
						let printBelowNinety = '84%';
						let printBelowFifty = '45%';
						let printBelowThirty = '28%';
						console.log(showLordOftheRings);
						console.log("Rotten Tomatoes Rating: " + printAboveRatings);
						console.log(showSilenceoftheLamb);
						console.log("Rotten Tomatoes Rating: " +printEqualRatings);
						console.log(showDearJohn);
						console.log("Rotten Tomatoes Rating: " +printBelowNinety);
						console.log(showHarryPotter);
						console.log("Rotten Tomatoes Rating: " +printBelowFifty);
						console.log(showDayAfterTomorrow);
						console.log("Rotten Tomatoes Rating: " +printBelowThirty);
					};
					/*console.log(nestedName);*/ //error
					//nestedName is not defined
					//nestedName variable, being declared in a nestedFunction cannot be accessed outside of the function it was declared in. because it is on the outside scope
					showRatings();
				};

				printMovieRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
};

printUsers();

/*console.log(friend1);
console.log(friend2);*/


